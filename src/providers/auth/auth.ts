import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import fAuthProvider = firebase.auth.AuthProvider;

import { Authenticate, User, RegisterInfo } from '../../models/user';

@Injectable()
export class AuthProvider {
  constructor(public ofAuth: AngularFireAuth, private afs: AngularFirestore) {}

  getAuthState() {
    return this.ofAuth.authState;
  }

  register(user: RegisterInfo) {
    return this.ofAuth.auth
      .createUserWithEmailAndPassword(user.email, user.password)
      .then(credential => {
        const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${credential.uid}`);

        const data: User = {
          uid: credential.uid,
          email: user.email,
          first_name: user.first_name,
          last_name: user.last_name,
        };

        userRef.set(data, { merge: true });

        return userRef.ref.get();
      })
      .then(data => data.data());
  }

  login(user: Authenticate) {
    return this.ofAuth.auth
      .signInWithEmailAndPassword(user.email, user.password)
      .then(credential => {
        const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${credential.uid}`);
        return userRef.ref.get();
      })
      .then(data => data.data());
  }

  logout() {
    return this.ofAuth.auth.signOut();
  }

  signInWithGoogle() {
    console.log('Sign in with google');
    return this.oauthSignIn(new firebase.auth.GoogleAuthProvider());
  }

  private oauthSignIn(provider: fAuthProvider) {
    if (!(<any>window).cordova) {
      return this.ofAuth.auth.signInWithPopup(provider);
    } else {
      return this.ofAuth.auth.signInWithRedirect(provider).then(() => {
        return this.ofAuth.auth
          .getRedirectResult()
          .then(result => {
            // This gives you a Google Access Token.
            // You can use it to access the Google API.
            let token = result.credential.accessToken;
            // The signed-in user info.
            let user = result.user;
            console.log(token, user);
          })
          .catch(function(error) {
            // Handle Errors here.
            alert(error.message);
          });
      });
    }
  }
}
