import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { Store, select } from '@ngrx/store';

import * as fromAuth from '../../reducers/auth';

import { User } from '../../models/user';

@Injectable()
export class UserProvider {
  user$: Observable<User>;
  _user: User;

  constructor(private afs: AngularFirestore, private store: Store<any>) {
    this.user$ = this.store.pipe(select(fromAuth.getUser));
    this.user$.subscribe((user: User) => {
      this._user = user;
    });
  }

  getUsersList() {
    if (this._user.uid) {
      const usersRef: AngularFirestoreCollection<any> = this.afs.collection<User>('users');
      return usersRef.stateChanges().map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as User;
          return data;
        });
      });
    } else {
      throw new Error('Not Authenticated!');
    }
  }
}
