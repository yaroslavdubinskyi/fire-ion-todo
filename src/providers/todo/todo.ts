import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { Store, select } from '@ngrx/store';

import * as fromAuth from '../../reducers/auth';

import { User } from '../../models/user';
import { Todolist } from '../../models/todolist';
import { Todo } from '../../models/todo';

@Injectable()
export class TodoProvider {
  user$: Observable<User>;
  _user: User;

  constructor(private afs: AngularFirestore, private store: Store<any>) {
    this.user$ = this.store.pipe(select(fromAuth.getUser));
    this.user$.subscribe((user: User) => {
      this._user = user;
    });
  }

  getCurrentUserTodolists() {
    if (this._user.uid) {
      const todolistsRef: AngularFirestoreCollection<any> = this.afs.collection<Todolist>(
        'todolists',
        ref => ref.where(`members.${this._user.uid}`, '==', true),
      );
      return todolistsRef.stateChanges();
    } else {
      throw new Error('Not Authenticated!');
    }
  }

  getCurrentUserTodolistTodos(id: string) {
    if (this._user.uid) {
      const todolistsRef: AngularFirestoreCollection<any> = this.afs
        .collection<Todolist>('todolists', ref =>
          ref.where(`members.${this._user.uid}`, '==', true),
        )
        .doc(id)
        .collection<Todo>('todos');

      return todolistsRef.stateChanges();
    } else {
      return Observable.throw('Not Authenticated!');
    }
  }

  createTodolist(todolist: Todolist) {
    if (this._user.uid) {
      const todolistId = this.afs.createId();
      const finalTodolist = {
        id: todolistId,
        title: todolist.title,
        members: { [this._user.uid]: true, ...todolist.members },
      };
      const tempTodo = {
        task: 'Write first todo...',
        completed: true,
      } as Todo;

      this.afs
        .collection<Todolist>('todolists')
        .doc(todolistId)
        .set(finalTodolist);
      this.afs.collection<Todo>(`todolists/${todolistId}/todos`).add(tempTodo);

      return Observable.of(true);
    } else {
      return Observable.throw('Not Authenticated!');
    }
  }

  removeTodolist(listId: string) {
    if (this._user.uid) {
      return Observable.fromPromise(
        this.afs
          .collection<Todolist>(`todolists`)
          .doc(listId)
          .delete(),
      );
    } else {
      return Observable.throw('Not Authenticated!');
    }
  }

  createTodo(listId: string, taskItem: Todo) {
    if (this._user.uid) {
      return Observable.fromPromise(
        this.afs.collection<Todo>(`todolists/${listId}/todos`).add(taskItem),
      );
    } else {
      return Observable.throw('Not Authenticated!');
    }
  }

  updateTodo(listId: string, taskId: string, taskItem: Partial<Todo>) {
    if (this._user.uid) {
      return Observable.fromPromise(
        this.afs
          .collection<Todo>(`todolists/${listId}/todos`)
          .doc(taskId)
          .update(taskItem),
      );
    } else {
      return Observable.throw('Not Authenticated!');
    }
  }

  removeTodo(listId: string, taskId: string) {
    if (this._user.uid) {
      return Observable.fromPromise(
        this.afs
          .collection<Todo>(`todolists/${listId}/todos`)
          .doc(taskId)
          .delete(),
      );
    } else {
      return Observable.throw('Not Authenticated!');
    }
  }
}
