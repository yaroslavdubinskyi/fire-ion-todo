import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store, select } from '@ngrx/store';

import * as fromAuth from '../../reducers/auth';
import * as Auth from '../../actions/auth';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loginForm: FormGroup;
  error$ = this.store.pipe(select(fromAuth.getError));
  isLoading$ = this.store.pipe(select(fromAuth.getLoading));

  constructor(private fb: FormBuilder, private store: Store<fromAuth.State>) {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  login() {
    if (this.loginForm.valid) {
      const user = {
        email: this.loginForm.value.email,
        password: this.loginForm.value.password,
      };
      this.store.dispatch(new Auth.Login(user));
    }
  }
}
