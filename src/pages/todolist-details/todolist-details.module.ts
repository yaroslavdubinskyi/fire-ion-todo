import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StoreModule } from '@ngrx/store';
import { TodolistDetailsPage } from './todolist-details';

@NgModule({
  declarations: [TodolistDetailsPage],
  imports: [IonicPageModule.forChild(TodolistDetailsPage), StoreModule],
  exports: [TodolistDetailsPage],
})
export class TodolistDetailsPageModule {}
