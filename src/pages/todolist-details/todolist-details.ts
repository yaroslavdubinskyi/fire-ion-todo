import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as TodoActions from '../../actions/todo';
import * as fromTodo from '../../reducers/todo';

import { Todo } from '../../models/todo';

@IonicPage()
@Component({
  selector: 'page-todolist-details',
  templateUrl: 'todolist-details.html',
})
export class TodolistDetailsPage {
  id: string;
  title: string;
  todos: Observable<Todo[]>;
  isLoading$: Observable<boolean>;

  constructor(
    private store: Store<fromTodo.State>,
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
  ) {
    this.id = navParams.get('todolist').id;
    this.title = navParams.get('todolist').title;
    this.todos = this.store.select(fromTodo.selectAll);
    this.isLoading$ = this.store.select(fromTodo.getLoading);
  }

  ionViewDidLoad() {
    this.store.dispatch(new TodoActions.ClearAll());
    this.store.dispatch(new TodoActions.Query(this.id));
  }

  toggleTodo(taskId: string, completed: boolean) {
    this.store.dispatch(
      new TodoActions.Update({
        listId: this.id,
        taskId,
        taskChanges: { completed: !completed },
      }),
    );
  }

  removeTodo(taskId: string) {
    const removeAlert = this.alertCtrl.create({
      title: 'Remove Task',
      message: 'Do You want to remove this task?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Remove',
          handler: () => {
            this.store.dispatch(
              new TodoActions.Remove({
                listId: this.id,
                taskId,
              }),
            );
          },
        },
      ],
    });
    removeAlert.present();
  }

  openCreateTodoPage() {
    this.navCtrl.push('CreateTodoPage', {
      listId: this.id,
    });
  }
}
