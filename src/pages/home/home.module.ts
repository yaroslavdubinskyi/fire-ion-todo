import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StoreModule } from '@ngrx/store';
import { HomePage } from './home';

@NgModule({
  declarations: [HomePage],
  imports: [IonicPageModule.forChild(HomePage), StoreModule],
  exports: [HomePage]
})
export class HomePageModule {}
