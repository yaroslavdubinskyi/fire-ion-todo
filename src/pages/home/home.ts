import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController } from 'ionic-angular';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as TodoList from '../../actions/todolist';
import * as fromTodoList from '../../reducers/todolist';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  todolists: Observable<any>;
  isLoading$: Observable<boolean>;

  constructor(
    private store: Store<fromTodoList.State>,
    public navCtrl: NavController,
    private alertCtrl: AlertController,
  ) {
    this.todolists = this.store.select(fromTodoList.selectAll);
    this.isLoading$ = this.store.select(fromTodoList.getLoading);
    this.store.dispatch(new TodoList.ClearAll());
    this.store.dispatch(new TodoList.Query());
  }

  openDetails(todolist) {
    this.navCtrl.push('TodolistDetailsPage', {
      todolist,
    });
  }

  removeTodolist(listId) {
    const removeAlert = this.alertCtrl.create({
      title: 'Remove Todolist',
      message: 'Do You want to remove this todolist?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Remove',
          handler: () => {
            this.store.dispatch(new TodoList.Remove(listId));
          },
        },
      ],
    });
    removeAlert.present();
  }

  openCreateTodolistPage() {
    this.navCtrl.push('CreateTodolistPage');
  }
}
