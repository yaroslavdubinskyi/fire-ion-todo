import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateTodolistPage } from './create-todolist';

@NgModule({
  declarations: [CreateTodolistPage],
  imports: [IonicPageModule.forChild(CreateTodolistPage)],
  exports: [CreateTodolistPage],
})
export class CreateTodolistPageModule {}
