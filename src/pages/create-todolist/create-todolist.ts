import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as fromTodolist from '../../reducers/todolist';
import * as fromUser from '../../reducers/user';
import * as TodolistActions from '../../actions/todolist';
import * as UserActions from '../../actions/user';
import { Todolist } from '../../models/todolist';
import { User } from '../../models/user';

@IonicPage()
@Component({
  selector: 'page-create-todolist',
  templateUrl: 'create-todolist.html',
})
export class CreateTodolistPage {
  users: Observable<User[]>;
  newTodolistForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private store: Store<fromTodolist.State>,
    public navCtrl: NavController,
  ) {
    this.newTodolistForm = this.fb.group({
      title: ['', Validators.required],
      members: [],
    });

    this.users = this.store.select(fromUser.getUsers);
  }

  ionViewDidLoad() {
    this.store.dispatch(new UserActions.GetUsers());
  }

  createNewTodolist() {
    if (this.newTodolistForm.valid) {
      const todolist = {
        title: this.newTodolistForm.value.title,
        members: this.newTodolistForm.value.members.reduce((o, val) => {
          o[val] = true;
          return o;
        }, {}),
      } as Todolist;
      this.store.dispatch(new TodolistActions.Create(todolist));
      this.navCtrl.pop();
    }
  }
}
