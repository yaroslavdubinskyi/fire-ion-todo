import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store, select } from '@ngrx/store';

import * as fromAuth from '../../reducers/auth';
import * as Auth from '../../actions/auth';
import { RegisterInfo } from '../../models/user';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  registerForm: FormGroup;
  error$ = this.store.pipe(select(fromAuth.getError));
  isLoading$ = this.store.pipe(select(fromAuth.getLoading));

  constructor(private fb: FormBuilder, private store: Store<fromAuth.State>) {
    this.registerForm = this.fb.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  register() {
    if (this.registerForm.valid) {
      const user = {
        first_name: this.registerForm.value.first_name,
        last_name: this.registerForm.value.last_name,
        email: this.registerForm.value.email,
        password: this.registerForm.value.password,
      } as RegisterInfo;
      this.store.dispatch(new Auth.Register(user));
    }
  }
}
