import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';

import * as fromTodo from '../../reducers/todo';
import * as TodoActions from '../../actions/todo';

import { Todo } from '../../models/todo';

@IonicPage()
@Component({
  selector: 'page-create-todo',
  templateUrl: 'create-todo.html',
})
export class CreateTodoPage {
  listId: string;
  newTodoForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    private store: Store<fromTodo.State>,
  ) {
    this.listId = navParams.get('listId');
    this.newTodoForm = this.fb.group({
      task: ['', Validators.required],
    });
  }

  createNewTodo() {
    if (this.newTodoForm.valid) {
      const taskItem = {
        task: this.newTodoForm.value.task,
        completed: false,
      } as Todo;
      this.store.dispatch(new TodoActions.Create({ listId: this.listId, taskItem }));
      this.navCtrl.pop();
    }
  }
}
