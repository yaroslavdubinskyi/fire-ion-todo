import { Action } from '@ngrx/store';

import { User } from '../models/user';

export enum UserActionTypes {
  GetUsers = '[Users] Get users',
  GetUsersSuccess = '[Users] Get users success',
  GetUsersError = '[Users] Get users error',
}

export class GetUsers implements Action {
  readonly type = UserActionTypes.GetUsers;
}

export class GetUsersSuccess implements Action {
  readonly type = UserActionTypes.GetUsersSuccess;
  constructor(public payload: User[]) {}
}

export class GetUsersError implements Action {
  readonly type = UserActionTypes.GetUsersError;
  constructor(public payload: string) {}
}

export type UserActions = GetUsers | GetUsersSuccess | GetUsersError;
