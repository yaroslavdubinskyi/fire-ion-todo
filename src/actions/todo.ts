import { Action } from '@ngrx/store';

import { Todo } from '../models/todo';

export enum TodoActionTypes {
  Query = '[Todos] Query todos',
  Added = '[Todos] added',
  Modified = '[Todos] modified',
  Removed = '[Todos] removed',
  ClearAll = '[Todos] Clear all',
  Create = '[Todos] Create',
  Update = '[Todos] Update',
  Remove = '[Todos] Remove',
}

export class Query implements Action {
  readonly type = TodoActionTypes.Query;
  constructor(public payload: string) {}
}

export class Added implements Action {
  readonly type = TodoActionTypes.Added;
  constructor(public payload: Todo) {}
}

export class Modified implements Action {
  readonly type = TodoActionTypes.Modified;
  constructor(public payload: Todo) {}
}

export class Removed implements Action {
  readonly type = TodoActionTypes.Removed;
  constructor(public payload: Todo) {}
}

export class ClearAll implements Action {
  readonly type = TodoActionTypes.ClearAll;
}

export class Update implements Action {
  readonly type = TodoActionTypes.Update;
  constructor(public payload: { listId: string; taskId: string; taskChanges: Partial<Todo> }) {}
}

export class Create implements Action {
  readonly type = TodoActionTypes.Create;
  constructor(public payload: { listId: string; taskItem: Todo }) {}
}

export class Remove implements Action {
  readonly type = TodoActionTypes.Remove;
  constructor(public payload: { listId: string; taskId: string }) {}
}

export type TodoActions = Query | Added | Modified | Removed | ClearAll | Update | Create | Remove;
