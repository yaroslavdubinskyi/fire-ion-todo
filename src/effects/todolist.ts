import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';
import { switchMap, mergeMap, map, catchError } from 'rxjs/operators';

import { TodoProvider } from '../providers/todo/todo';
import {
  QueryError,
  TodolistActionTypes,
  Create,
  Remove,
  CreateSuccess,
} from '../actions/todolist';

@Injectable()
export class TodolistEffects {
  @Effect()
  query$: Observable<Action> = this.actions$.ofType(TodolistActionTypes.Query).pipe(
    switchMap(action => {
      return this.todoProv.getCurrentUserTodolists();
    }),
    mergeMap(actions => actions),
    map((action: any) => {
      return {
        type: `[Todolists] ${action.type}`,
        payload: { id: action.payload.doc.id, ...action.payload.doc.data() },
      };
    }),
    catchError(error => Observable.of(new QueryError(error.message))),
  );

  @Effect()
  create$: Observable<Action> = this.actions$.ofType(TodolistActionTypes.Create).pipe(
    map((action: Create) => action.payload),
    switchMap(todolist => {
      return this.todoProv.createTodolist(todolist);
    }),
    map(() => new CreateSuccess()),
  );

  @Effect({ dispatch: false })
  remove$: Observable<Action> = this.actions$.ofType(TodolistActionTypes.Remove).pipe(
    map((action: Remove) => action.payload),
    switchMap(listId => {
      return this.todoProv.removeTodolist(listId);
    }),
  );

  constructor(private actions$: Actions, private todoProv: TodoProvider) {}
}
