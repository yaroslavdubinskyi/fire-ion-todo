import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { tap, map, exhaustMap, catchError } from 'rxjs/operators';

import { AuthProvider } from '../providers/auth/auth';
import {
  Login,
  LoginSuccess,
  LoginFailure,
  Register,
  RegisterSuccess,
  RegisterFailure,
  AuthActionTypes,
} from '../actions/auth';
import { User, RegisterInfo, Authenticate } from '../models/user';

@Injectable()
export class AuthEffects {
  @Effect()
  login$ = this.actions$.pipe(
    ofType(AuthActionTypes.Login),
    map((action: Login) => action.payload),
    exhaustMap((auth: Authenticate) =>
      Observable.fromPromise(this.authProvider.login(auth)).pipe(
        map((user: User) => new LoginSuccess(user)),
        catchError(error => of(new LoginFailure(error.message))),
      ),
    ),
  );

  @Effect({ dispatch: false })
  logout$ = this.actions$.pipe(
    ofType(AuthActionTypes.Logout),
    tap(() => this.authProvider.logout()),
  );

  @Effect()
  register$ = this.actions$.pipe(
    ofType(AuthActionTypes.Register),
    map((action: Register) => action.payload),
    exhaustMap((auth: RegisterInfo) =>
      Observable.fromPromise(this.authProvider.register(auth)).pipe(
        map((user: User) => new RegisterSuccess(user)),
        catchError(error => of(new RegisterFailure(error.message))),
      ),
    ),
  );

  constructor(private actions$: Actions, private authProvider: AuthProvider) {}
}
