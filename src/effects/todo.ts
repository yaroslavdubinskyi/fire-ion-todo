import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';
import { switchMap, mergeMap, map } from 'rxjs/operators';

import { TodoProvider } from '../providers/todo/todo';
import { Query, Update, Create, Remove, TodoActionTypes } from '../actions/todo';

@Injectable()
export class TodoEffects {
  @Effect()
  query$: Observable<Action> = this.actions$.ofType(TodoActionTypes.Query).pipe(
    map((action: Query) => action.payload),
    switchMap(action => {
      return this.todoProv.getCurrentUserTodolistTodos(action);
    }),
    mergeMap(actions => actions),
    map((action: any) => {
      return {
        type: `[Todos] ${action.type}`,
        payload: { id: action.payload.doc.id, ...action.payload.doc.data() },
      };
    }),
  );

  @Effect({ dispatch: false })
  create$: Observable<Action> = this.actions$.ofType(TodoActionTypes.Create).pipe(
    map((action: Create) => action.payload),
    switchMap(data => {
      return this.todoProv.createTodo(data.listId, data.taskItem);
    }),
  );

  @Effect({ dispatch: false })
  update$: Observable<Action> = this.actions$.ofType(TodoActionTypes.Update).pipe(
    map((action: Update) => action.payload),
    switchMap(data => {
      return this.todoProv.updateTodo(data.listId, data.taskId, data.taskChanges);
    }),
  );

  @Effect({ dispatch: false })
  remove$: Observable<Action> = this.actions$.ofType(TodoActionTypes.Remove).pipe(
    map((action: Remove) => action.payload),
    switchMap(data => {
      return this.todoProv.removeTodo(data.listId, data.taskId);
    }),
  );

  constructor(private actions$: Actions, private todoProv: TodoProvider) {}
}
