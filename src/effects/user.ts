import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';
import { switchMap, map, catchError } from 'rxjs/operators';

import { UserProvider } from '../providers/user/user';
import { UserActionTypes, GetUsersSuccess, GetUsersError } from '../actions/user';

@Injectable()
export class UserEffects {
  @Effect()
  getUsers$: Observable<Action> = this.actions$.ofType(UserActionTypes.GetUsers).pipe(
    switchMap(action => {
      return this.userProv.getUsersList();
    }),
    map(users => new GetUsersSuccess(users)),
    catchError(err => Observable.of(new GetUsersError(err))),
  );

  constructor(private actions$: Actions, private userProv: UserProvider) {}
}
