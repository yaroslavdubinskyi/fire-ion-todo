import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { StoreModule, ActionReducer, MetaReducer } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { StorageSyncEffects, storageSync } from 'ngrx-store-ionic-storage';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule, AngularFirestore } from 'angularfire2/firestore';

import { ReactiveFormsModule } from '@angular/forms';

import { FIREBASE_CONFIG } from '../helpers/firebase.config';

import { MyApp } from './app.component';
import { AuthProvider } from '../providers/auth/auth';

import { reducers } from '../reducers/index';
import { AuthEffects } from '../effects/auth';
import { TodolistEffects } from '../effects/todolist';
import { TodoEffects } from '../effects/todo';
import { UserEffects } from '../effects/user';
import { TodoProvider } from '../providers/todo/todo';
import { UserProvider } from '../providers/user/user';

export function onSyncError(err) {
  console.log(err);
}

export const storageSyncReducer = storageSync({
  keys: ['auth'],
  hydratedStateKey: 'hydrated',
  onSyncError: onSyncError,
});

export function storageMetaReducer(reducer: ActionReducer<any>): ActionReducer<any, any> {
  return storageSyncReducer(reducer);
}

export const metaReducers: MetaReducer<any, any>[] = [storageMetaReducer];

@NgModule({
  declarations: [MyApp],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    StoreModule.forRoot(reducers, {
      metaReducers,
      initialState: {
        hydrated: false,
      },
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
    }),
    EffectsModule.forRoot([
      StorageSyncEffects,
      AuthEffects,
      TodolistEffects,
      TodoEffects,
      UserEffects,
    ]),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireAuthModule,
    AngularFirestoreModule,
    ReactiveFormsModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthProvider,
    TodoProvider,
    UserProvider,
  ],
})
export class AppModule {
  constructor(private afs: AngularFirestore) {
    const settings = { timestampsInSnapshots: true };
    afs.app.firestore().settings(settings);
  }
}
