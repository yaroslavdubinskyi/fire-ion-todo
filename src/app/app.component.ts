import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Store, select } from '@ngrx/store';
import * as fromAuth from '../reducers/auth';
import * as Auth from '../actions/auth';

import { User } from '../models/user';

import { FirstRunPage, LoggedInRunPage } from '../pages/pages';

@Component({
  templateUrl: 'app.html',
})
export class MyApp {
  rootPage = FirstRunPage;
  isLoggedIn$ = this.store.select(fromAuth.getLoggedIn);
  currUser$ = this.store.select(fromAuth.getUser);
  _currUser: User;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private store: Store<fromAuth.State>,
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      this.initializeApp();
    });
  }
  initializeApp() {
    this.currUser$.subscribe(user => {
      this._currUser = user;
    });
    this.isLoggedIn$.subscribe(loggedIn => {
      if (loggedIn) {
        this.rootPage = LoggedInRunPage;
      } else {
        this.rootPage = FirstRunPage;
      }
    });
  }

  logout() {
    this.store.dispatch(new Auth.Logout());
  }
}
