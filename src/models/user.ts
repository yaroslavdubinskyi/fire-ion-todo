export interface User {
  uid: string;
  email: string;
  display_name?: string;
  first_name?: string;
  last_name?: string;
}

export interface Authenticate {
  email: string;
  password: string;
}

export interface RegisterInfo {
  email: string;
  password: string;
  first_name: string;
  last_name: string;
}
