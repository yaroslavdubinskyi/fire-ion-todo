import { ActionReducerMap } from '@ngrx/store';
import { authReducer } from './auth';
import { todolistReducer } from './todolist';
import { todoReducer } from './todo';
import { usersReducer } from './user';

export const reducers: ActionReducerMap<any> = {
  auth: authReducer,
  todolist: todolistReducer,
  todo: todoReducer,
  users: usersReducer,
};
