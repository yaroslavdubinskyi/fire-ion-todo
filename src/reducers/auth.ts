import { createSelector, createFeatureSelector } from '@ngrx/store';
import { AuthActions, AuthActionTypes } from '../actions/auth';
import { User } from '../models/user';

export interface State {
  isLoggedIn: boolean;
  isLoading: boolean;
  error: string | null;
  user: User;
}

export const initialState: State = {
  isLoggedIn: false,
  isLoading: false,
  error: null,
  user: {
    uid: '',
    email: '',
    first_name: '',
    last_name: '',
  },
};

export function authReducer(state = initialState, action: AuthActions): State {
  switch (action.type) {
    case AuthActionTypes.Login: {
      return {
        ...state,
        isLoading: true,
        error: null,
      };
    }
    case AuthActionTypes.LoginSuccess: {
      return {
        ...state,
        isLoading: false,
        isLoggedIn: true,
        user: action.payload,
      };
    }
    case AuthActionTypes.LoginFailure: {
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    }
    case AuthActionTypes.Logout: {
      return initialState;
    }
    case AuthActionTypes.Register: {
      return {
        ...state,
        isLoading: true,
        error: null,
      };
    }
    case AuthActionTypes.RegisterSuccess: {
      return {
        ...state,
        isLoading: false,
        isLoggedIn: true,
        user: action.payload,
      };
    }
    case AuthActionTypes.RegisterFailure: {
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    }

    default: {
      return state;
    }
  }
}

export const selectAuthState = createFeatureSelector<State>('auth');

export const getUser = createSelector(selectAuthState, (state: State) => state.user);

export const getUserId = createSelector(selectAuthState, (state: State) => state.user.uid);

export const getLoggedIn = createSelector(selectAuthState, (state: State) => state.isLoggedIn);

export const getLoading = createSelector(selectAuthState, (state: State) => state.isLoading);

export const getError = createSelector(selectAuthState, (state: State) => state.error);
