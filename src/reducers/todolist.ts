import { createSelector, createFeatureSelector } from '@ngrx/store';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { TodolistActions, TodolistActionTypes } from '../actions/todolist';
import { Todolist } from '../models/todolist';

// Entity adapter
export const todolistAdapter = createEntityAdapter<Todolist>();
export interface State extends EntityState<Todolist> {
  error: string;
  creating: boolean;
  isLoading: boolean;
  newTodoList: Todolist;
}

export const initialState: State = todolistAdapter.getInitialState({
  error: '',
  creating: false,
  isLoading: false,
  newTodoList: {
    id: '',
    title: '',
  },
});

export function todolistReducer(state: State = initialState, action: TodolistActions) {
  switch (action.type) {
    case TodolistActionTypes.Query: {
      return {
        ...state,
        error: '',
        isLoading: true,
      };
    }

    case TodolistActionTypes.QueryError: {
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };
    }

    case TodolistActionTypes.Added:
      return todolistAdapter.addOne(action.payload, { ...state, isLoading: false });

    case TodolistActionTypes.Modified:
      return todolistAdapter.updateOne(
        {
          id: action.payload.id,
          changes: action.payload,
        },
        state,
      );

    case TodolistActionTypes.Removed:
      return todolistAdapter.removeOne(action.payload.id, state);

    case TodolistActionTypes.ClearAll:
      return todolistAdapter.removeAll(state);

    case TodolistActionTypes.Create:
      return {
        ...state,
        creating: true,
        newTodoList: action.payload,
      };

    case TodolistActionTypes.CreateSuccess:
      return {
        ...state,
        creating: false,
      };

    case TodolistActionTypes.CreateError:
      return {
        ...state,
        creating: false,
        error: action.payload,
      };

    default:
      return state;
  }
}

export const getTodolistState = createFeatureSelector<State>('todolist');

export const getError = createSelector(getTodolistState, (state: State) => state.error);

export const getLoading = createSelector(getTodolistState, (state: State) => state.isLoading);

export const { selectIds, selectEntities, selectAll, selectTotal } = todolistAdapter.getSelectors(
  getTodolistState,
);
